local SellJunk = CreateFrame("Frame")

SellJunk:RegisterEvent("MERCHANT_SHOW")

function SellJunk:OnEvent()
	--SellJunk:Print(event)
	if (event == "MERCHANT_SHOW") then
		SellJunk:Sell()
	end
end
SellJunk:SetScript("OnEvent", SellJunk.OnEvent)

function SellJunk:Sell()
	for bag = 0,4 do
		for slot = 1,GetContainerNumSlots(bag) do
			local item = GetContainerItemLink(bag,slot)
			if item then
				if string.find(item,"|cff9d9d9d") then
					UseContainerItem(bag,slot)
					SellJunk:Print("Sold " .. item)
				end
			end
		end
	end
end

function SellJunk:Print( argument )
	DEFAULT_CHAT_FRAME:AddMessage(argument)
end