# SellJunk

Sell all your Junk items automatically and don't lose any more time on it.

## Getting Started

SellJunk is a world of warcraft Addon that sells all junk items in your inventory whenever you open a vendor's dialog screen.

### Prerequisites

World of Warcraft 1.12.1 client

### Installing

[CLI]
cd /worldofwarcraftpath/interface/addons
git clone https://AmadorRosa@bitbucket.org/AmadorRosa/selljunk.git

[ZIP]
Unzip the content in: /Your wow path/interface/addons


## Versioning

1.0

## Authors

* **Amador Rosa** - (https://bitbucket.org/AmadorRosa)