## Interface: 11200
## Title: SellJunk
## Notes: This addon sells all junk items when a vendor window is opened
## Version: 1.0
## Author: Failon
## eMail: amador.rosa@anyma.io
## URL: anyma.io
SellJunk.lua